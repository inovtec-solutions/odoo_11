# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from odoo import fields, models
from datetime import datetime



class EDValidationOfCable(models.Model):
    _name = 'ed.validation.of.cable'
    _description = 'Electricity Validation of Cable'
    _inherit = ['mail.thread', 'mail.activity.mixin']
    
    name = fields.Char('Name')
    validation_process = fields.Selection([('contractor','Contractor'),('ed cable jointers','ED Cable Jointers')], string="Validation of Jointing Process done for")
    validation_type = fields.Selection([('normal validation','Normal Validation'),('re-validation','Re-Validation')], string="Type of Validation done")
    state = fields.Selection([('draft','Draft'),('submitted','Submit'),('reviewed','Reviewed')],default='draft',track_visibility='onchange')
    details_of_cable_joint = fields.Selection([('mv','MV'),('lv','LV')], string="Type of Scheme/Fault")
    source_substation=fields.Char("Source Substation",size=200)
    destination_substation=fields.Char("Destination Substation",size=200)
    feeder_details=fields.Char("Feeder Details",size=500)
    reason_of_cable_joint = fields.Selection([('fault','Fault'),('scheme','Scheme')],string="Reason")
    fault_report_no = fields.Char(string="Fault Report No", size=100)
    scheme_no = fields.Char(string="Scheme No", size=100)
    reason_date= fields.Date(string='Date', required=True,default=datetime.now())
    active = fields.Boolean('Active', default=True)
    submitted_by = fields.Integer("Submitted By")
    submitted_date = fields.Datetime("Submitted Date")
    reviewed_by = fields.Integer("Reviewed By")
    reviewed_date = fields.Datetime("Reviewed Date")
    
    def set_to_submit(self):
        self.write({'state':'submitted','submitted_by': self._uid,'submitted_date':datetime.now()})
        
    def set_to_reviewed(self):
        self.write({'state':'reviewed','reviewed_by':self._uid,'reviewed_date':datetime.now()})
        
    def set_to_draft(self):
        self.write({'state':'draft'})
  
    