# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from odoo import fields, models, api, _
from datetime import datetime, timedelta, date
from odoo.exceptions import AccessError, UserError, RedirectWarning, ValidationError, Warning


class EDValidationByKMTemplate(models.Model):
    _name='ed.validation.by.km.template'
    _description='Validation By KM Template.'
    
    name = fields.Char('Template Name', size=300, required=True)
    effect_from = fields.Date('W.E.F', required=True)
    effect_to = fields.Date('W.E.T')
    active=fields.Boolean('Active',default=True)
    validation_by_km_template_lines = fields.One2many('ed.validation.by.km.template.line','validation_by_km_template_id',string="Validation By KM Template Lines", required=True)
    
    @api.model
    def create(self, vals):
        if self.search([('effect_from','<=',vals['effect_from']),('effect_to','>=',vals['effect_from']),('active','in',[True,False])]):
            raise UserError(_("Conflict in W.E.F Date"))
        
        if self.search([('effect_from','>',vals['effect_from']),('active','=',True)]):
            raise UserError(_("Invalid Date selected, Conflict in dates found"))
                   
        effect_to_date = (datetime.strptime(vals['effect_from'],'%Y-%m-%d') - timedelta(days=1)).date()

        old_templates = self.search([('effect_from','<',vals['effect_from']),('active','=',True)]) 
        
        #old_templates.write({'active':False,'effect_to' : effect_to_date})
        for t in old_templates:
            t.write({'active':False,'effect_to' : effect_to_date})
            
        template = super(EDValidationByKMTemplate, self).create(vals)
        return template

    def _update_template_dates(self,effect_from,last_from_date, last_to_date):
        old_templates = self.search([('effect_from','=',last_from_date),('effect_to','=',last_to_date),('active','=',False)]) 
        effect_to_date = (datetime.strptime(effect_from,'%Y-%m-%d').date()-timedelta(days=1))

        for t in old_templates:
            t.write({'effect_to' : effect_to_date})

    @api.multi
    def write(self, vals):
        #for r in self:
        
        if 'effect_from' in vals:
            effect_from = vals['effect_from']
            cr = self.env.cr
            cr.execute("SELECT MAX(effect_from),MAX(effect_to) FROM ed_validation_by_km_template_line WHERE active=false")
            row = cr.fetchone()
            if row:
                last_from_date = row[0]
                last_to_date = row[1]
                
                # if effect from date is less than latest closed Template.
                if effect_from <= last_from_date:
                    raise UserError(_('Invalid W.E.F date selected'))
                            
                # if effect_from is between "Effect From Date" and "Effect To Date" last closed Template
                elif effect_from > last_from_date and effect_from <= last_to_date:
                    # check if any notice issued between New W.E.F and last closed W.E.T date
                    if self.env['ed.violation.notice'].search([('date','>=',effect_from),('date','<=',last_to_date)]):
                        raise UserError(_("Can not update W.E.F date because VIOLATION NOTICE exists"))
                    else:
                        self._update_template_dates(effect_from,last_from_date,last_to_date)
 
                elif effect_from > last_to_date:
                    self._update_template_dates(effect_from,last_from_date,last_to_date)
                                  
        template = super(EDValidationByKMTemplate, self).write(vals)
        return template
  
class EDValidationByKMTemplateLine(models.Model):
    _name = 'ed.validation.by.km.template.line'
    _description='Violation Notice Template Lines.'
       
    name = fields.Char("Name", size=300)
    validation_by_km_template_id = fields.Many2one('ed.validation.by.km.template','Validation By KM',required=True)

class EDValidationByKM(models.Model):
    _name = 'ed.validation.by.km'
    _description = 'Electricity Violation Details'
    _inherit = ['mail.thread', 'mail.activity.mixin']
    
    def _get_validation_km_template(self):
        result = []
        for template in self.env['ed.validation.by.km.template'].search([('active','=',True)]):
            for template_line in template.validation_by_km_template_lines:
                result.append((0,0, {'name' : template_line.name}))
        return result
    
    def _get_template_id(self):
        records = self.env['ed.validation.by.km.template'].search([('active','=',True)])
        if records:
            return records[0]
        else:
            raise UserError(_("No active Template found, Kindly first create Template"))
        
    name = fields.Char(string="Type of Work")
    date = fields.Date(string="Re-Validation Date")
    observation = fields.Char(string='Observation / Comments')
    areas_of_improvement= fields.Char(string='Areas of Improvement needed')
    state = fields.Selection([('draft','Draft'),('submitted','Submit'),('reviewed','Reviewed')],default='draft',track_visibility='onchange')
    active = fields.Boolean('Active', default=True)
    submitted_by = fields.Integer("Submitted By")
    submitted_date = fields.Datetime("Submitted Date")
    reviewed_by = fields.Integer("Reviewed By")
    reviewed_date = fields.Datetime("Reviewed Date")
    photographs = fields.Many2many('ir.attachment', string="Attachments")    
    validation_by_km_lines = fields.One2many('ed.validation.by.km.line','validation_by_km_id', string="Violation Notice Lines", default=_get_validation_km_template)
    validation_by_km_template_id=fields.Many2one('ed.validation.by.km.template','Template',default=_get_template_id)
    
    def set_to_submit(self):
        self.write({'state':'submitted','submitted_by': self._uid,'submitted_date':datetime.now()})
        
    def set_to_reviewed(self):
        self.write({'state':'reviewed','reviewed_by':self._uid,'reviewed_date':datetime.now()})
        
    def set_to_draft(self):
        self.write({'state':'draft'})
        
class EDValidationByKMLine(models.Model):
    _name = 'ed.validation.by.km.line'
    _description='Violation Notice Lines.'
       
    name = fields.Char("Name",size=300, Required=True)
    is_selected=fields.Boolean('Yes/No')
    validation_by_km_id = fields.Many2one('ed.validation.by.km','Validation By KM')
        
    
    
    
    