# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from odoo import fields, models, api, _
from datetime import datetime, timedelta, date
from odoo.exceptions import AccessError, UserError, RedirectWarning, ValidationError, Warning


class EDViolationNoticeTemplate(models.Model):
    _name='ed.violation.notice.template'
    _description='Violation Notice Template.'
    
    name = fields.Char('Template Name', size=300, required=True)
    effect_from = fields.Date('W.E.F', required=True)
    effect_to = fields.Date('W.E.T')
    active=fields.Boolean('Active',default=True)
    violation_notice_template_lines = fields.One2many('ed.violation.notice.template.line','violation_notice_template_id',string="Violation Notice Template Lines", required=True)
    
    @api.model
    def create(self, vals):
        if self.search([('effect_from','<=',vals['effect_from']),('effect_to','>=',vals['effect_from']),('active','in',[True,False])]):
            raise UserError(_("Conflict in W.E.F Date"))
        
        if self.search([('effect_from','>',vals['effect_from']),('active','=',True)]):
            raise UserError(_("Invalid Date selected, Conflict in dates found"))
                   
        effect_to_date = (datetime.strptime(vals['effect_from'],'%Y-%m-%d') - timedelta(days=1)).date()

        old_templates = self.search([('effect_from','<',vals['effect_from']),('active','=',True)]) 
        
        #old_templates.write({'active':False,'effect_to' : effect_to_date})
        for t in old_templates:
            t.write({'active':False,'effect_to' : effect_to_date})
            
        template = super(EDViolationNoticeTemplate, self).create(vals)
        return template

    def _update_template_dates(self,effect_from,last_from_date, last_to_date):
        old_templates = self.search([('effect_from','=',last_from_date),('effect_to','=',last_to_date),('active','=',False)]) 
        effect_to_date = (datetime.strptime(effect_from,'%Y-%m-%d').date()-timedelta(days=1))

        for t in old_templates:
            t.write({'effect_to' : effect_to_date})

    @api.multi
    def write(self, vals):
        #for r in self:
        
        if 'effect_from' in vals:
            effect_from = vals['effect_from']
            cr = self.env.cr
            cr.execute("SELECT MAX(effect_from),MAX(effect_to) FROM ed_violation_notice_template WHERE active=false")
            row = cr.fetchone()
            if row:
                last_from_date = row[0]
                last_to_date = row[1]
                
                # if effect from date is less than latest closed Template.
                if effect_from <= last_from_date:
                    raise UserError(_('Invalid W.E.F date selected'))
                            
                # if effect_from is between "Effect From Date" and "Effect To Date" last closed Template
                elif effect_from > last_from_date and effect_from <= last_to_date:
                    # check if any notice issued between New W.E.F and last closed W.E.T date
                    if self.env['ed.violation.notice'].search([('date','>=',effect_from),('date','<=',last_to_date)]):
                        raise UserError(_("Can not update W.E.F date because VIOLATION NOTICE exists"))
                    else:
                        self._update_template_dates(effect_from,last_from_date,last_to_date)
 
                elif effect_from > last_to_date:
                    self._update_template_dates(effect_from,last_from_date,last_to_date)
                                  
        template = super(EDViolationNoticeTemplate, self).write(vals)
        return template
  
class EDViolationNoticeTemplateLine(models.Model):
    _name = 'ed.violation.notice.template.line'
    _description='Violation Notice Template Lines.'
       
    name = fields.Char("Name", size=300)
    violation_notice_template_id = fields.Many2one('ed.violation.notice.template','Violation Notice',required=True)

class EDViolationNotice(models.Model):
    _name = 'ed.violation.notice'
    _description = 'Electricity Violation Details'
    _inherit = ['mail.thread', 'mail.activity.mixin']
    
    def _get_violation_notice_template(self):
        result = []
        for template in self.env['ed.violation.notice.template'].search([('active','=',True)]):
            for template_line in template.violation_notice_template_lines:
                result.append((0,0, {'name' : template_line.name}))
        return result
    
    def _get_template_id(self):
        records = self.env['ed.violation.notice.template'].search([('active','=',True)])
        if records:
            return records[0]
        else:
            raise UserError(_("No active Template found, Kindly first create Template"))
    
    date = fields.Date(string='Date', required=True,default=datetime.now())
    violation_no = fields.Integer(string='Violation No',required=True)
    sis_no = fields.Char(string='SIS No')
    name = fields.Char(string="Type of Work")
    contract_no = fields.Integer(string='Contract No')
    contractor_id = fields.Many2one("res.partner",string="Contractor")
    location = fields.Char(string='Site / Location')
    authority = fields.Char(string='Authority')
    observation = fields.Char(string='Observation / Comments')
    state = fields.Selection([('draft','Draft'),('submitted','Submit'),('reviewed','Reviewed')],default='draft',track_visibility='onchange')
    active = fields.Boolean('Active', default=True)
    submitted_by = fields.Integer("Submitted By")
    submitted_date = fields.Datetime("Submitted Date")
    reviewed_by = fields.Integer("Reviewed By")
    reviewed_date = fields.Datetime("Reviewed Date")
    attachment = fields.Binary("Attachment")
    image = fields.Many2many('ir.attachment', string="Attachments")
    violation_notice_lines = fields.One2many('ed.violation.notice.line','violation_notice_id', string="Violation Notice Lines", default=_get_violation_notice_template)
    violation_notice_template_id=fields.Many2one('ed.violation.notice.template','Template',default=_get_template_id)
    
    def set_to_submit(self):
        self.write({'state':'submitted','submitted_by': self._uid,'submitted_date':datetime.now()})
        
    def set_to_reviewed(self):
        self.write({'state':'reviewed','reviewed_by':self._uid,'reviewed_date':datetime.now()})
        
    def set_to_draft(self):
        self.write({'state':'draft'})
        
class EDViolationNoticeLine(models.Model):
    _name = 'ed.violation.notice.line'
    _description='Violation Notice Lines.'
       
    name = fields.Char("Name",size=300, Required=True)
    is_selected=fields.Boolean('Yes/No')
    remarks=fields.Char('Remarks',size=300)
    violation_notice_id = fields.Many2one('ed.violation.notice','Violation Notice')
        
    
    
    
    