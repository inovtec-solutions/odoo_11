# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from odoo import fields, models, api, _
from datetime import datetime, timedelta, date
from odoo.exceptions import AccessError, UserError, RedirectWarning, ValidationError, Warning



class EDExcavationInspectionAndCallOrderRelease(models.Model):
    _name = 'ed.excavation.inspection.cor'
    _description = 'Excavation Inspection and Call off Order Release'
    _inherit = ['mail.thread', 'mail.activity.mixin']
       
    date = fields.Date(string='Date', required=True,default=datetime.now())
    gtc_no = fields.Integer(string='GTC No',required=True)
    name = fields.Char(string="Title of Contract")
    contractor_id = fields.Many2one("res.partner",string="Contractor")
    cwr_no = fields.Char(string='CWR No',size=100)
    parent_work_order_no = fields.Char(string='Parent Work Order No',size=100)
    child_work_order_no = fields.Char(string='Child Work Order No',size=100)
    scheme_no = fields.Char(string='Scheme Number / Fault Report No',size=100)
    substation_no = fields.Char(string='Substation No / Ckt Identification',size=100)
    location = fields.Char(string='Location',size=200)
    type_of_scheme = fields.Selection([('MV','MV'),('LV','LV')],string="Type of Scheme/Fault")
    detail_of_scheme = fields.Char(string="Details of Scheme / Fault / Maintenance Work",size=500)
    is_trench_excavation_approved = fields.Boolean(string="Trench Excavation Approved",default=False),
    length_of_trench = fields.Float(string="Length of trench")
    length_of_exising_road = fields.Float(string="Length of New Ducts Required (Existing Road)")
    length_of_future_road = fields.Float(string="Length of New Ducts Required (Future Road)")
    state = fields.Selection([('draft','Draft'),('submitted','Submit'),('reviewed','Reviewed')],default='draft',track_visibility='onchange')
    active = fields.Boolean('Active', default=True)
    submitted_by = fields.Integer("Submitted By")
    submitted_date = fields.Datetime("Submitted Date")
    reviewed_by = fields.Integer("Reviewed By")
    reviewed_date = fields.Datetime("Reviewed Date")    
    excavation_inspection_cor_lines = fields.One2many('ed.excavation.inspection.cor.line','excavation_inspection_cor_id', string="Call of Order Release")
    comments = fields.Char(string="Comments", size=500)

    
    def set_to_submit(self):
        self.write({'state':'submitted','submitted_by': self._uid,'submitted_date':datetime.now()})
        
    def set_to_reviewed(self):
        self.write({'state':'reviewed','reviewed_by':self._uid,'reviewed_date':datetime.now()})
        
    def set_to_draft(self):
        self.write({'state':'draft'})
        
class EDExcavationInspectionAndCallOrderReleaseLine(models.Model):
    _name = 'ed.excavation.inspection.cor.line'
    _description='Violation Notice Lines.'
    
    @api.onchange('name')
    def on_product_change(self):
        if not self.name:
            return {}


        for product in self.name:
            for t in product.product_tmpl_id:
                self.description=t.description
        self.quantity=1
   
    name = fields.Many2one('product.product','Product', required=True)
    description = fields.Char(string="Description",size=500)
    quantity=fields.Float('Quantity')
    excavation_inspection_cor_id = fields.Many2one('ed.excavation.inspection.cor','Product')
        
    
    
    
    