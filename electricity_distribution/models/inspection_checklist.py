# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from odoo import fields, models, api, _
from datetime import datetime, timedelta, date
from odoo.exceptions import AccessError, UserError, RedirectWarning, ValidationError, Warning


class EDInspectionChecklistTemplate(models.Model):
    _name='ed.inspection.checklist.template'
    _description='Inspection Checklist Template.'
    
    name = fields.Char('Template Name', size=300, required=True)
    effect_from = fields.Date('W.E.F', required=True)
    effect_to = fields.Date('W.E.T')
    active=fields.Boolean('Active',default=True)
    inspection_checklist_template_lines = fields.One2many('ed.inspection.checklist.template.line','inspection_checklist_template_id',string="Inspection Checklist Template Lines", required=True)
    
    @api.model
    def create(self, vals):
        if self.search([('effect_from','<=',vals['effect_from']),('effect_to','>=',vals['effect_from']),('active','in',[True,False])]):
            raise UserError(_("Conflict in W.E.F Date"))
        
        if self.search([('effect_from','>',vals['effect_from']),('active','=',True)]):
            raise UserError(_("Invalid Date selected, Conflict in dates found"))
                   
        effect_to_date = (datetime.strptime(vals['effect_from'],'%Y-%m-%d') - timedelta(days=1)).date()

        old_templates = self.search([('effect_from','<',vals['effect_from']),('active','=',True)]) 
        
        #old_templates.write({'active':False,'effect_to' : effect_to_date})
        for t in old_templates:
            t.write({'active':False,'effect_to' : effect_to_date})
            
        template = super(EDInspectionChecklistTemplate, self).create(vals)
        return template

    def _update_template_dates(self,effect_from,last_from_date, last_to_date):
        old_templates = self.search([('effect_from','=',last_from_date),('effect_to','=',last_to_date),('active','=',False)]) 
        effect_to_date = (datetime.strptime(effect_from,'%Y-%m-%d').date()-timedelta(days=1))

        for t in old_templates:
            t.write({'effect_to' : effect_to_date})

    @api.multi
    def write(self, vals):
        #for r in self:
        
        if 'effect_from' in vals:
            effect_from = vals['effect_from']
            cr = self.env.cr
            cr.execute("SELECT MAX(effect_from),MAX(effect_to) FROM ed_inspection_checklist_template WHERE active=false")
            row = cr.fetchone()
            if row:
                last_from_date = row[0]
                last_to_date = row[1]
                
                # if effect from date is less than latest closed Template.
                if effect_from <= last_from_date:
                    raise UserError(_('Invalid W.E.F date selected'))
                            
                # if effect_from is between "Effect From Date" and "Effect To Date" last closed Template
                elif effect_from > last_from_date and effect_from <= last_to_date:
                    # check if any notice issued between New W.E.F and last closed W.E.T date
                    if self.env['ed.violation.notice'].search([('date','>=',effect_from),('date','<=',last_to_date)]):
                        raise UserError(_("Can not update W.E.F date because INSPECTION CHECKLIST is in use"))
                    else:
                        self._update_template_dates(effect_from,last_from_date,last_to_date)
 
                elif effect_from > last_to_date:
                    self._update_template_dates(effect_from,last_from_date,last_to_date)
                                  
        template = super(EDInspectionChecklistTemplate, self).write(vals)
        return template
  
class EDInspectionChecklistTemplateLine(models.Model):
    _name = 'ed.inspection.checklist.template.line'
    _description='Inspection Checklist Template Lines.'
       
    name = fields.Char("Name", size=300)
    inspection_checklist_template_id = fields.Many2one('ed.inspection.checklist.template','Inspection Checklist',required=True)

class EDInspectionChecklist(models.Model):
    _name = 'ed.inspection.checklist'
    _description = 'Electricity Inspection Checklist Details'
    _inherit = ['mail.thread', 'mail.activity.mixin']
    
    def _get_inspection_checklist_template(self):
        result = []
        for template in self.env['ed.inspection.checklist.template'].search([('active','=',True)]):
            for template_line in template.inspection_checklist_template_lines:
                result.append((0,0, {'name' : template_line.name}))
        return result
    
    def _get_template_id(self):
        records = self.env['ed.inspection.checklist.template'].search([('active','=',True)])
        if records:
            return records[0]
        else:
            raise UserError(_("No active Template found, Kindly first create Checklist Template"))
    
    date = fields.Date(string='Date', required=True,default=datetime.now())
    sis_no = fields.Char(string='SIS No')
    name = fields.Char(string="Name", size=200)
    position= fields.Char(string="Position", size=200)
    mobile_no= fields.Char(string='Mobile No')
    client_detail_id = fields.Many2one("res.partner",string="Client Details")
    contractor_id = fields.Many2one("res.partner",string="Contractor Supervisor")        
    state = fields.Selection([('draft','Draft'),('submitted to inspector','Submit to Inspector'),('submitted to engineer','Submitted to Engineer'),('reviewed','Reviewed')],default='draft',track_visibility='onchange')
    active = fields.Boolean('Active', default=True)
    inspector_remarks = fields.Char('Inspector Remarks / Comments', size=500)
    has_violation_notice=fields.Boolean('Is Violation Notice Issued?')
    other_action=fields.Char('Any other action deemed necessary', size=500)
    submitted_to_inspector_by = fields.Integer("Submitted By")
    submitted_to_inspector_date = fields.Datetime("Submitted Date")
    submitted_to_engineer_by = fields.Integer("Submitted By")
    submitted_to_engineer_date = fields.Datetime("Submitted Date")
    reviewed_by = fields.Integer("Reviewed By")
    reviewed_date = fields.Datetime("Reviewed Date")
    inspection_checklist_lines = fields.One2many('ed.inspection.checklist.line','inspection_checklist_id', string="Inspection Checklist Lines", default=_get_inspection_checklist_template)
    inspection_checklist_template_id=fields.Many2one('ed.inspection.checklist.template','Template',default=_get_template_id)
    
    def set_submit_to_inspector(self):
        self.write({'state':'submitted to inspector','submitted_to_inspector_by': self._uid,'submitted_to_inspector_date':datetime.now()})

    def set_submit_to_engineer(self):
        self.write({'state':'submitted to engineer','submitted_to_engineer_by': self._uid,'submitted_to_engineer_date':datetime.now()})
        
    def set_to_reviewed(self):
        self.write({'state':'reviewed','reviewed_by':self._uid,'reviewed_date':datetime.now()})
        
    def set_to_draft(self):
        self.write({'state':'draft'})
        
class EDInspectionChecklistLine(models.Model):
    _name = 'ed.inspection.checklist.line'
    _description='Inspector Checklist Lines.'
       
    name = fields.Char("Name",size=300, Required=True)
    is_selected=fields.Boolean('Yes/No')
    remarks=fields.Char('Remarks',size=300)
    inspection_checklist_id = fields.Many2one('ed.inspection.checklist','Inspector Checklist')
        
    
    
    
    