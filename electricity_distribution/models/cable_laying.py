# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from odoo import fields, models, api, _
from datetime import datetime, timedelta, date
from odoo.exceptions import AccessError, UserError, RedirectWarning, ValidationError, Warning

class EDCableLayingTemplate(models.Model):
    _name='ed.cable.laying.template'
    _description='Cable Laying Template.'
    
    name = fields.Char('Template Name', size=300, required=True)
    effect_from = fields.Date('W.E.F', required=True)
    effect_to = fields.Date('W.E.T')
    active=fields.Boolean('Active',default=True)
    cable_laying_template_lines = fields.One2many('ed.cable.laying.template.line','cable_laying_template_id',string="Cable Laying Template Lines", required=True)
    state = fields.Selection([('draft','Draft'),('submitted','Submitted'),('approved','Approved')])
    @api.model
    def create(self, vals):
        if self.search([('effect_from','<=',vals['effect_from']),('effect_to','>=',vals['effect_from']),('active','in',[True,False])]):
            raise UserError(_("Conflict in W.E.F Date"))
        
        if self.search([('effect_from','>',vals['effect_from']),('active','=',True)]):
            raise UserError(_("Invalid Date selected, Conflict in dates found"))
                   
        effect_to_date = (datetime.strptime(vals['effect_from'],'%Y-%m-%d') - timedelta(days=1)).date()

        old_templates = self.search([('effect_from','<',vals['effect_from']),('active','=',True)]) 
        
        #old_templates.write({'active':False,'effect_to' : effect_to_date})
        for t in old_templates:
            t.write({'active':False,'effect_to' : effect_to_date})
            
        template = super(EDCableLayingTemplate, self).create(vals)
        return template

    def _update_template_dates(self,effect_from,last_from_date, last_to_date):
        old_templates = self.search([('effect_from','=',last_from_date),('effect_to','=',last_to_date),('active','=',False)]) 
        effect_to_date = (datetime.strptime(effect_from,'%Y-%m-%d').date()-timedelta(days=1))

        for t in old_templates:
            t.write({'effect_to' : effect_to_date})

    @api.multi
    def write(self, vals):
        #for r in self:
        
        if 'effect_from' in vals:
            effect_from = vals['effect_from']
            cr = self.env.cr
            cr.execute("SELECT MAX(effect_from),MAX(effect_to) FROM ed_cable_laying_template WHERE active=false")
            row = cr.fetchone()
            if row:
                last_from_date = row[0]
                last_to_date = row[1]
                
                # if effect from date is less than latest closed Template.
                if effect_from <= last_from_date:
                    raise UserError(_('Invalid W.E.F date selected'))
                            
                # if effect_from is between "Effect From Date" and "Effect To Date" last closed Template
                elif effect_from > last_from_date and effect_from <= last_to_date:
                    # check if any notice issued between New W.E.F and last closed W.E.T date
                    if self.env['ed.cable.laying'].search([('date','>=',effect_from),('date','<=',last_to_date)]):
                        raise UserError(_("Can not update W.E.F date because Cable Laying exists"))
                    else:
                        self._update_template_dates(effect_from,last_from_date,last_to_date)
 
                elif effect_from > last_to_date:
                    self._update_template_dates(effect_from,last_from_date,last_to_date)
                                  
        template = super(EDCableLayingTemplate, self).write(vals)
        return template
  
class EDCableLayingTemplateLine(models.Model):
    _name = 'ed.cable.laying.template.line'
    _description='Cable Lying Template Lines.'
       
    name = fields.Char("Name", size=300)
    cable_laying_template_id = fields.Many2one('ed.cable.laying.template','Cable Laying',required=True)


class EDCableLaying(models.Model):
    _name = 'ed.cable.laying'
    _description = 'Excavation Inspection and Call off Order Release'
    _inherit = ['mail.thread', 'mail.activity.mixin']
    _inherit ='ed.excavation.inspection.cor'
       
    def _get_cable_laying_template(self):
        result = []
        for template in self.env['ed.cable.laying.template'].search([('active','=',True)]):
            for template_line in template.cable_laying_template_lines:
                result.append((0,0, {'name' : template_line.name}))
        return result
    
    def _get_template_id(self):
        records = self.env['ed.cable.laying.template'].search([('active','=',True)])
        if records:
            return records[0]
        else:
            raise UserError(_("No active Template found, Kindly first create Template"))
     
    length_of_cable= fields.Float(string="Length of Cable")
    active = fields.Boolean('Active', default=True)
    cable_laying_lines = fields.One2many('ed.cable.laying.line','cable_laying_id', string="Cable Laying Lines", default=_get_cable_laying_template)
    cable_laying_template_id=fields.Many2one('ed.cable.laying.template','Template',default=_get_template_id)
    length_of_reinstatement = fields.Float(string="Length of Reinstatement")
    work_completed_satisfactorily = fields.Selection([('Yes','Yes'),('No','No')], string="Work Completed Satisfactoryly")
    work_delayed_by_contractor = fields.Selection([('Yes','Yes'),('No','No')],string="Work Delayed by Contractor")
        
class EDCableLayingLine(models.Model):
    _name = 'ed.cable.laying.line'
    
    name = fields.Char("Name",size=300, Required=True)
    description = fields.Char(string="Description",size=500)
    is_verified=fields.Boolean('Yes / No')
    cable_laying_id = fields.Many2one('ed.cable.laying','Product')
        
    
    
    
    