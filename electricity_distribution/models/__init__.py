# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from . import violation_notice
from . import inspection_checklist
from . import excavation_inspection_and_cor
from . import cable_laying
from . import validation_cable
from . import validation_by_km
