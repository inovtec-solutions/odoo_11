# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.
{
    'name' : 'Electricity Distribution Management',
    'version' : '0.1',
    'sequence': 165,
    'category': 'Electricity',
    'website' : 'https://www.odoo.com/page/fleet',
    'summary' : 'Electricity Distribution Management',
    'description' : """
    Electricity Distribution Management
        - Manage Violation Notice...
        - Manage Inspection Check List...
        - Manage Excavation Inspection and Call off order Release...
        - Manage Cable laying, back filling and Reinstatement Inspection
        - Manage Validation of Cable Jointing Process
""",
    'depends': ['base','mail','resource', 'product'],
    'data': [
        'security/ED_security.xml',
        'security/ir.model.access.csv',
        'views/violation_notice_view.xml',
        'views/inspection_checklist.xml',
        'views/excavation_inspection_cor_view.xml',
        'views/cable_laying_view.xml',
        'views/validation_cable_view.xml',
        'views/validation_by_km_view.xml',
        'views/ED_menu_view.xml'],
    'demo': [],

    'installable': True,
    'application': True,
}
